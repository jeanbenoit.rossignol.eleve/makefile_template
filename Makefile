#--PROJECT_NAME--#
PROJECT_NAME	= 

#--C FILES--# > all .c files
C_FILES			= $(notdir $(wildcard src/*.c))

#--C FILES TO O FILES--# > where you make the .o files dependencies
O_FILES			= $(C_FILES:.c=.o)

#--NAME--# > name of the project
NAME			= $(PROJECT_NAME)

#--FLAGS--# > flags used by the command above
ERROR_FLAGS		= -Werror -Wall -Wextra -g

#--DIR PATH--# > path to the file
SRC_DIR			= src/
OBJ_DIR			= obj/
INC_DIR			= include/

#--PREFIX--#
PRE_SRC			= $(addprefix $(SRC_DIR), $(C_FILES))
PRE_OBJ			= $(addprefix $(OBJ_DIR), $(O_FILES))

#--VPATH--#
VPATH			= $(SRC_DIR)

#--ACTIONS--# > all the thing you want your Makefile to do
$(OBJ_DIR)%.o:		%.c
				@mkdir -p obj
				@gcc $(ERROR_FLAGS) -o $@ -c $<

$(NAME):			$(PRE_OBJ)
				@echo "Compiling $(PROJECT_NAME)..."
				@gcc $(ERROR_FLAGS) $(PRE_OBJ) -o $(NAME)
				@echo "Compiling $(PROJECT_NAME) done."

all:				$(NAME)

clean:
				@echo "Removing $(PROJECT_NAME) object files..."
				@rm -f $(PRE_OBJ)
				@rm -rf $(OBJ_DIR)
				@echo "Removing $(PROJECT_NAME) object files done."

fclean:				clean
				@echo "Removing $(PROJECT_NAME) program..."
				@rm -f $(NAME)
				@echo "Removing $(PROJECT_NAME) program done."

re:					fclean all

.PHONY: all fclean clean re
